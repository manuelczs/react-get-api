import { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Main from './components/Main';
import Navbar from './components/Navbar';
import Content from './components/Content';
import Form from './components/Form';
import Products from './components/Products';
import { v4 } from 'uuid';

const App = () => {
  const API_URL = 'https://fakestoreapi.com/products';

  const [products, setProducts] = useState([]);

  const getProductsFromApi = async () => {
    try {
      const response = await axios.get(API_URL)
      setProducts(response.data)
    } catch (err) {
      console.err(`Error: ${err}`);
    }
  }

  useEffect(() => { getProductsFromApi() }, []);

  const setForm = (formData) => {
    formData = { ...formData, id: v4() };
    setProducts([formData, ...products]);
  }

  return (
    <div className="App">
      <Router>
        <Main>
          <Navbar />
          <Switch>

            <Route exact path="/">
              <Content>
                <Products products={products} />
              </Content>
            </Route>

            <Route exact path="/form">
              <Content>
                <Form sendForm={setForm} />
              </Content>
            </Route>

          </Switch>
        </Main>
      </Router>
    </div>
  );
}

export default App;
