import './Content.css';
import ScrollToTop from 'react-scroll-up';

const Content = ({ children }) => (
  <div className="content">
    {children}

    <ScrollToTop showUnder={600}>
      <span className="up-button">UP!</span>
    </ScrollToTop>
  </div>
)

export default Content;
