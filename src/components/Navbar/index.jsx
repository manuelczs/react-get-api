import { Link } from 'react-router-dom';
import './Navbar.css';

const Navbar = () => {
  return <nav className="main-nav">
    <Link to="/">
      <li>Home</li>
    </Link>
    <Link to="/form">
      <li>Add new</li>
    </Link>
  </nav>
}

export default Navbar;
