import { useState } from 'react';
import './Form.css';
import FormContainer from './FormContainer';
import Formx from './Formx';
import InputForm from './InputForm';
import Error from '../Error';

const Form = ({ sendForm }) => {
  // using exampleImg to load a new Product by example. Remove this line to use in production
  const exampleImg = 'https://fakestoreapi.com/img/71kWymZ+c+L._AC_SX679_.jpg';

  // Replace exampleImg by '' to use in production
  const [formValues, setFormValues] = useState({ title: '', price: '', description: '', category: '', image: exampleImg });
  const [error, setError] = useState({ message: null });

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValues({ ...formValues, [name]: value });
  }

  const checkFields = () => {
    return (Object.values(formValues).includes(''));
  }

  const submitForm = (e) => {
    e.preventDefault();
    if (!checkFields()) {
      sendForm(formValues);
      // Replace exampleImg by '' to use in production
      setFormValues({ title: '', price: '', description: '', category: '', image: exampleImg });
      setError({ ...error, message: null })
    } else {
      setError({ ...error, message: 'All fields must be completed.' });
    }
  }

  return (
    <Formx>
      <FormContainer submit={submitForm}>
        { error.message ? <Error message={error.message} /> : null }
        <InputForm
          type="text"
          name="title"
          handler={handleChange}
          value={formValues.title}
        />

        <InputForm
          type="text"
          name="price"
          handler={handleChange}
          value={formValues.price}
        />

        <InputForm
          type="text"
          name="description"
          handler={handleChange}
          value={formValues.description}
        />

        <InputForm
          type="text"
          name="category"
          handler={handleChange}
          value={formValues.category}
        />

        <InputForm
          type="text"
          name="image"
          tagName="Image URL"
          handler={handleChange}
          // Replace exampleImg by formValues.image in production
          value={exampleImg}
          disabled={false}
        />

        {/* Submit button */}
        <div className="form-input">
          <input className="input input-button"
            type="submit"
            value="Submit"
          />
        </div>

      </FormContainer>
    </Formx>
  )
}

export default Form;
