import './FormContainer.css'

const FormContainer = ({ children, submit }) => (
  <form onSubmit={(e) => submit(e)}>

    <p>Add a new product</p>
    {children}

  </form>
)

export default FormContainer;
