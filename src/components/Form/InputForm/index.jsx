import './InputForm.css';

const InputForm = ({ type, name, tagName, value, handler, placeholder, disabled }) => (

  <div className="form-input">

    <label htmlFor={name}>
      <p className="form-input-tagname">
        { tagName || name[0].toUpperCase() + name.slice(1, name.length) }
      </p>
    </label>
    <br/>

    <input className="input"
      type={type}
      name={name}
      id={name}
      value={value}
      onChange={(e) => handler(e)}
      disabled={disabled}
      placeholder={placeholder}
    />

  </div>
);

export default InputForm;
