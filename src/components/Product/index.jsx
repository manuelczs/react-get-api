import './Product.css';

const Product = ({ title, price, description, category, image }) => (
  <div className="product-container">
    <div className="img-container">
      <img src={image} alt=""></img>
    </div>
    <div className="img-side">
        <h2>{title}</h2>
        <p className="description">{description}</p>
        <p className="category">Category: {category}</p>
        <p className="price">${price}</p>
    </div>
  </div>
)

export default Product;
