import Product from '../Product';
import './Products.css';

const Products = ({ products }) => (
  <div className="products-container">
    {
      products.map(
        (product, key) =>
          <Product title={product.title}
            price={product.price}
            description={product.description}
            category={product.category}
            image={product.image}
            key={key}
          />
      )
    }
  </div>
)

export default Products;
